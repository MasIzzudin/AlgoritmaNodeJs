// Logic 2 Soal 8

var a = 16
var b = a%2==0 ? a+1 : a;
for(let i=0; i < b; i++){
    for(let j=0; j < b; j++){
        if(i == j){
            document.write("<button class='btn-xs pink'>" + (i + 1 + j) + "</button>")
        }else if(i + j == 15){
            document.write("<button class='btn-xs pink'>" + j*2 + "</button>")
        }else if(j <= i && j <= 15-(i+1)){
            document.write("<button class='btn-xs'>A</button>")
        }else if(j >= i && j >= 16-(i+1)){
            document.write("<button class='btn-xs'>B</button>")
        }else{
            document.write("<button class='btn-xs dark'>&nbsp</button>")
        }
    }document.write("<br>")
}