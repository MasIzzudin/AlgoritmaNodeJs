document.write("==========================")
document.write("Soal 9 Logic 3")
document.write("==========================")
document.write("<br><br>")

var a;
function fib(a){
    return a<2 ? a : fib(a-1) + fib(a-2)
}

function cekGenap(a){
    if (a%2) {
        return Math.ceil(a/2)
    }else{
        return "&nbsp"
    }
}

// function bagi(a){
//     return Math.ceil(fib(a/2));
// }

var n = 9

for(var x=0; x < n; x++){
    for(var y=0; y < n; y++){
        if (x <= y && x <= n-(y+1)) {
            document.write(`<button class='btn-xs'>${cekGenap(x+1)}</button>`)
        }else if( x >= y && x >= n-(y+1)){
            document.write(`<button class='btn-xs'>${cekGenap(n-x)}</button>`)
        }else if( y <= x && y <= n-(x+1)){
            document.write(`<button class='btn-xs'>${cekGenap(y+1)}</button>`)
        }else if( y >= x && y >= n-(x+1)){
            document.write(`<button class='btn-xs'>${cekGenap(n-y)}</button>`)
        }else{
            document.write("&nbsp")
        }
    }document.write("<br>")
}
