document.write("==========================")
document.write("Soal 8 Logic 3")
document.write("==========================")
document.write("<br><br>")

var n = 18
n = (n%2==0) ? n : n+1

function cekGenap(a){
    return (a%2) ? a/2 : "&nbsp"
}

for(var x=1; x < n; x++){
    for(var y=1; y < n; y++){
        if (x <= y && x <= n-y) {
            document.write(`<button class='btn-xs'>${cekGenap(x+1)}</button>`)
          }else if(x >= y && x >= n-y){
            document.write(`<button class='btn-xs'>${cekGenap(n-x)}</button>`)
          }else if (y <= x && y <= n-x) {
            document.write(`<button class='btn-xs'>${cekGenap(y+1)}</button>`)
          }else if ( y >= x && x >= n-y) {
            document.write(`<button class='btn-xs'>${cekGenap(n-y)}</button>`)
          }else{
            document.write("<button class='btn-xs pink'>&nbsp</button>")
          }
    }document.write("<br>")
}
document.write("<br><br>")