document.write("==========================")
document.write("Soal 7 Logic 3")
document.write("==========================")
document.write("<br><br>")

var a;
function fib(a){
    return a<2 ? a : fib(a-1) + fib(a-2)
}


var n = 9
n = (n%2==1) ? n : n+1
for(var x=0; x < n; x++){
  for(var y=0; y < n; y++){
    if (x <= y && x <= n - (y+1)) {
      document.write(`<button class='btn-xs'>${fib(x+1)}</button>`)
    }else if(x >= y && x >= n - (y+1)){
      document.write(`<button class='btn-xs'>${fib(n-x)}</button>`)
    }else if (y <= x && y <= n - (x+1)) {
      document.write(`<button class='btn-xs pink'>${fib(y+1)}</button>`)
    }else if ( y >= x && x >= n - (y+1)) {
      document.write(`<button class='btn-xs pink'>${fib(n-y)}</button>`)
    }else{
      document.write("&nbsp")
    }
  }document.write("<br>")
}

document.write("<br><br>")