document.write("==========================")
document.write("Soal 10 Logic 3")
document.write("==========================")
document.write("<br><br>")

var a;
function fib(a){
    return a<2 ? a : fib(a-1) + fib(a-2)
}

function cekGenap(a){
    return (a%2) ? fib(Math.ceil(a/2)) : `<span class='btn-xs pink'>${cekHuruf((a/2)-1)}</span>`
}

function cekHuruf(a){
    var huruf = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("")
    return huruf[a]
}

var n = 3
n = (n%2) ? n : n+1
n = (n%4 == 1) ? n : n+((n%4)-1)

for(var x=0; x < n; x++){
    for(var y=0; y < n; y++){
        if (x <= y && x <= n-(y+1)) {
            document.write(`<button class='btn-xs'>${cekGenap(x+1)}</button>`)
        }else if( x >= y && x >= n-(y+1)){
            document.write(`<button class='btn-xs'>${cekGenap(n-x)}</button>`)
        }else if( y <= x && y <= n-(x+1)){
            document.write(`<button class='btn-xs'>${cekGenap(y+1)}</button>`)
        }else if( y >= x && y >= n-(x+1)){
            document.write(`<button class='btn-xs'>${cekGenap(n-y)}</button>`)
        }else{
            document.write("&nbsp")
        }
    }document.write("<br>")
}
